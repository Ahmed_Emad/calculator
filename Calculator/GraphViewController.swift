//
//  ViewController.swift
//  FunctionDrawer
//
//  Created by Ahmed Barakat on 102 / 12 / 17.
//  Copyright © 2017 Ahmed Barakat. All rights reserved.
//

import UIKit

class GraphViewController: UIViewController {
    
    @IBOutlet weak var graphView: GraphView! {
        didSet {
            let pinchRecognizer = UIPinchGestureRecognizer(
                target: graphView, action: #selector(GraphView.scaleGraph(byReactingTo:)))
            graphView.addGestureRecognizer(pinchRecognizer)
            
            let panRecognizer = UIPanGestureRecognizer(
                target: graphView, action: #selector(GraphView.moveOrigin(byReactingTo:)))
            graphView.addGestureRecognizer(panRecognizer)
            
            let doubleTapRecognizer = UITapGestureRecognizer(
                target: graphView, action: #selector(GraphView.setOrigin(byReactingTo:)))
            doubleTapRecognizer.numberOfTapsRequired = 2
            graphView.addGestureRecognizer(doubleTapRecognizer)
        }
    }
    
    var drawFunction: ((Double) -> Double)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // TODO: Please remove !
        graphView!.drawFunction = self.drawFunction
    }
    
}

