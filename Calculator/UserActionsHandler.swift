//
//  ActionsHandler.swift
//  Calculator
//
//  Created by Ahmed Barakat on 100 / 10 / 17.
//  Copyright © 2017 Ahmed Barakat. All rights reserved.
//

import Foundation

//Completed: Rename Struct.
struct UserActionsHandler {
    
    private var undoStack = Stack<UserAction>()
    private var redoStack = Stack<UserAction>()
    
    func peek() -> UserAction? {
        return undoStack.peek()
    }
    
    mutating func addAction(_ action: UserAction) {
        undoStack.push(action)
    }
    
    //Completed: Rename the method.
    mutating func removeLast() -> UserAction? {
        return undoStack.pop()
    }
    
    mutating func clear() {
        undoStack.clear()
        redoStack.clear()
    }
    
    @discardableResult
    mutating func undo() -> UserAction? {
        guard let lastAction = undoStack.pop() else {
            return nil
        }
        redoStack.push(lastAction)
        return lastAction
    }
    
    @discardableResult
    mutating func redo() -> UserAction? {
        guard let lastAction = redoStack.pop() else {
            return nil
        }
        undoStack.push(lastAction)
        return lastAction
    }
    
}
