//
//  Stack.swift
//  Calculator
//
//  Created by Ahmed Barakat on 99 / 9 / 17.
//  Copyright © 2017 Ahmed Barakat. All rights reserved.
//

import Foundation

struct Stack<Element> {
    
    var items = [Element]()
    
    mutating func push(_ item: Element) {
        items.append(item)
    }
    
    mutating func pop() -> Element? {
        if isEmpty() { return nil }
        else { return items.removeLast() }
    }
    
    func peek() -> Element? {
        return items.last
    }
    
    mutating func clear() {
        items = []
    }
    
    func isEmpty() -> Bool {
        return items.isEmpty
    }
    
}
