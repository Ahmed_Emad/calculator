//
//  Action.swift
//  Calculator
//
//  Created by Ahmed Barakat on 99 / 9 / 17.
//  Copyright © 2017 Ahmed Barakat. All rights reserved.
//

import Foundation
//Completed: 1. Use expressive name instead Action. 2. When we should use indirect enum?
//indirect means that we will use the enum in itself (recursive)
indirect enum UserAction {
    
    case value(Double)
    case variable(String)
    case constant(Double, String)
    case unaryAction(UserAction, (Double) -> Double, (String) -> String)
    case binaryAction(UserAction, PendingBinaryAction)
    
    //Completed: The use of “get” is unnecessary
    var description: String {
        switch self {
        case let .value(value):
            return String(value)
        case let .variable(name):
            return name
        case let .constant(_, description):
            return description
        case let .unaryAction(action, _, descriptionGenerator):
            let operand = action.description
            return descriptionGenerator(operand)
        case let .binaryAction(secondAction, pendingBinaryAction):
            let firstOperandDescription = pendingBinaryAction.firstAction.description
            let secondOperandDescription = secondAction.description
            let descriptionGenerator = pendingBinaryAction.descriptionGenerator
            return descriptionGenerator(firstOperandDescription, secondOperandDescription)
        }
    }
    
    //Completed: The use of “get” is unnecessary, unless one or more values are returned indirectly.
    //https://developer.apple.com/library/content/documentation/Cocoa/Conceptual/CodingGuidelines/Articles/NamingMethods.html
    func result(with variables: Dictionary<String,Double>? = nil) -> Double {
        switch self {
        case let .value(value):
            return value
        case let .variable(name):
            return variables?[name] ?? 0.0
        case let .constant(value, _):
            return value
        //Completed: Don't write "Gen", Please write the whole word.
        case let .unaryAction(action, valueGenerator, _):
            let value = action.result(with: variables)
            return valueGenerator(value)
        case let .binaryAction(secondAction, pendingBinaryAction):
            let firstOperandValue = pendingBinaryAction.firstAction.result(with: variables)
            let secondOperandValue = secondAction.result(with: variables)
            let valueGenerator = pendingBinaryAction.function
            return valueGenerator(firstOperandValue, secondOperandValue)
        }
    }
    
}
