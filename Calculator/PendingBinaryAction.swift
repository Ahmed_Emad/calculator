//
//  PendingBinaryAction.swift
//  Calculator
//
//  Created by Ahmed Barakat on 100 / 10 / 17.
//  Copyright © 2017 Ahmed Barakat. All rights reserved.
//

import Foundation

struct PendingBinaryAction {
    
    let firstAction: UserAction
    let description: String
    let function: (Double, Double) -> Double
    //Completed: Rename
    let descriptionGenerator: (String, String) -> String
    
}
