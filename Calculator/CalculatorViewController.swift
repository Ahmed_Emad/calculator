//
//  ViewController.swift
//  Calculator
//
//  Created by Ahmed Barakat on 92 / 2 / 17.
//  Copyright © 2017 Ahmed Barakat. All rights reserved.
//

import UIKit

class CalculatorViewController: UIViewController, UISplitViewControllerDelegate {
    
    private static var undoStack = Stack<Character>()
    
    @IBOutlet weak var resultDisplay: UILabel!
    @IBOutlet weak var descriptionDisplay: UILabel!
    @IBOutlet weak var mDisplay: UILabel!
    
    var isTypig: Bool = false
    
    var displayValue: Double {
        get {
            if let text = resultDisplay.text, let value = Double(text) {
                return value
            } else {
                return 0.0
            }
        }
        set {
            resultDisplay.text = String(newValue)
        }
    }
    
    private var brain = CalculatorBrain()
    
    private var variables: [String: Double] = [String: Double]()
    
    private var mValue: Double = 0.0 {
        didSet {
            mDisplay.text = String(mValue)
            variables["M"] = mValue
        }
    }
    
    
    override func awakeFromNib() {
        self.splitViewController?.delegate = self
    }
    
    func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController: UIViewController, onto primaryViewController: UIViewController) -> Bool {
        guard primaryViewController.contents == self,
            let graphVC = secondaryViewController.contents as? GraphViewController,
            graphVC.drawFunction == nil else {
                return false
        }
        return true
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        return (!isTypig && !brain.evaluate().isPending && brain.lastActionInfo != nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let graphVC = segue.destination.contents as? GraphViewController else {
            return
        }
        graphVC.drawFunction = brain.lastActionInfo?.function
        graphVC.title = brain.lastActionInfo?.description
    }
    
    private func updateDisplays(using result: (result: Double?, isPending: Bool, description: String),
                                withValue updateValue: Bool = true) {
        if updateValue {
            if let value = result.result {
                displayValue = value
            } else {
                displayValue = 0
            }
        }
        let description = result.description + (result.isPending ? "..." : "=")
        descriptionDisplay.text = description
    }
    
    private func undoTyping() {
        guard let resultText = resultDisplay.text else {
            return
        }
        let endIndex = resultText.index(before: resultText.endIndex)
        resultDisplay.text = resultText.substring(to: endIndex)
        if let resultText = resultDisplay.text, resultText == "" {
            isTypig = false
        }
    }
    
    @IBAction func touchDigit(_ sender: UIButton) {
        let digit = sender.currentTitle!
        if isTypig {
            let currentText = resultDisplay.text!
            resultDisplay.text = currentText + digit
        } else {
            resultDisplay.text = digit
            isTypig = true
            let result = brain.evaluate()
            updateDisplays(using: result, withValue: false)
        }
    }
    
    @IBAction func performOperation(_ sender: UIButton) {
        if isTypig {
            brain.setOperand(displayValue)
            isTypig = false
        }
        guard let mathmaticalSymbol = sender.currentTitle else {
            return
        }
        brain.performOperation(mathmaticalSymbol)
        let result = brain.evaluate(using: variables)
        updateDisplays(using: result)
    }
    
    @IBAction func handleVariable(_ sender: UIButton) {
        guard let symbol = sender.currentTitle else {
            return
        }
        isTypig = false
        switch symbol {
        case "M":
            brain.setOperand(variable: "M")
            resultDisplay.text = "M"
        case "→M":
            mValue = displayValue
            let result = brain.evaluate(using: variables)
            updateDisplays(using: result)
        default:
            break
        }
    }
    
    @IBAction func handleStack(_ sender: UIButton) {
        guard let symbol = sender.currentTitle else {
            return
        }
        switch symbol {
        case "↩︎":
            if isTypig {
                undoTyping()
            } else {
                brain.undo()
            }
        case "↪︎":
            brain.redo()
        default:
            break
        }
        if !isTypig {
            let result = brain.evaluate(using: variables)
            updateDisplays(using: result)
        }
    }
    
}


extension UIViewController {
    var contents: UIViewController {
        if let navcon = self as? UINavigationController {
            return navcon.visibleViewController ?? self
        } else {
            return self
        }
    }
}
