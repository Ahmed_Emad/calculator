//
//  GraphView.swift
//  FunctionDrawer
//
//  Created by Ahmed Barakat on 102 / 12 / 17.
//  Copyright © 2017 Ahmed Barakat. All rights reserved.
//

import UIKit

@IBDesignable
class GraphView: UIView {
    
    @IBInspectable
    var scale: CGFloat = 20.0 {
        didSet {
            setNeedsDisplay()
        }
    }
    @IBInspectable
    var lineWidth: CGFloat = 2.0 {
        didSet {
            setNeedsDisplay()
        }
    }
    @IBInspectable
    var drawColor: UIColor = UIColor.blue {
        didSet {
            setNeedsDisplay()
        }
    }
    
    var drawFunction: ((Double) -> Double)? {
        didSet {
            setNeedsDisplay()
        }
    }
    var originToCenter = CGPoint(x: 0, y: 0)  {
        didSet {
            setNeedsDisplay()
        }
    }
    
    private  var origin: CGPoint  {
        get {
            var origin = originToCenter
            origin.x += graphCenter.x
            origin.y += graphCenter.y
            return origin
        }
        set {
            var origin = newValue
            origin.x -= graphCenter.x
            origin.y -= graphCenter.y
            originToCenter = origin
        }
    }
    
    private var axesDrawer = AxesDrawer(color: UIColor.black)
    private var graphCenter: CGPoint {
        return convert(center, from: superview)
    }
    private var maxYValue: CGFloat = 0.0
    private var minYValue: CGFloat = 0.0
    private var snapshot:UIView?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.setNeedsDisplay()
    }
    
    override func draw(_ rect: CGRect) {
        axesDrawer.contentScaleFactor = contentScaleFactor
        axesDrawer.drawAxes(in: bounds, origin: origin, pointsPerUnit: scale)
        drawfunction(in: bounds, origin: origin, pointsPerUnits: scale)
    }
    
    func drawfunction(in rect: CGRect, origin: CGPoint, pointsPerUnits: CGFloat) {
        drawColor.set()

        let path = UIBezierPath()
        var xValue = bounds.minX
        
        while xValue <= bounds.maxX {
            //TODO: What if pointsPerUnits = 0 ?
            let scaledXValue = (xValue - origin.x) / pointsPerUnits
            
            if let yValue = drawFunction?(Double(scaledXValue)) {
                let point = CGPoint(x: scaledXValue, y: CGFloat(yValue))
                let scaledPoint = CGPoint(x: (point.x * pointsPerUnits) + origin.x,
                                          y: origin.y - (point.y * pointsPerUnits))
                if rect.contains(scaledPoint) {
                    if !path.isEmpty {
                        path.addLine(to: scaledPoint)
                    }
                    path.move(to: scaledPoint)
                }
                
                if maxYValue < point.y { maxYValue = point.y }
                if minYValue > point.y { minYValue = point.y }
            }
            xValue += 1 / contentScaleFactor
        }
        path.stroke()
    }
    
    func scaleGraph(byReactingTo pinchRecognizer: UIPinchGestureRecognizer) {
        switch pinchRecognizer.state {
        case .began:
            snapshot = self.snapshotView(afterScreenUpdates: false)
            //TODO: What if snapshot = nil, Don't use ! without checking. [check all cases in this class]
            snapshot!.alpha = 0.5
            self.addSubview(snapshot!)
        case .changed:
            let touch = pinchRecognizer.location(in: self)
            snapshot!.frame.size.height *= pinchRecognizer.scale
            snapshot!.frame.size.width *= pinchRecognizer.scale
            snapshot!.frame.origin.x = snapshot!.frame.origin.x * pinchRecognizer.scale + (1 - pinchRecognizer.scale) * touch.x
            snapshot!.frame.origin.y = snapshot!.frame.origin.y * pinchRecognizer.scale + (1 - pinchRecognizer.scale) * touch.y
            pinchRecognizer.scale = 1.0
        case .ended:
            let newScale = snapshot!.frame.width / self.frame.width
            scale *= newScale
            origin.x = origin.x * newScale + snapshot!.frame.origin.x
            origin.y = origin.y * newScale + snapshot!.frame.origin.y
            snapshot!.removeFromSuperview()
            //TODO: snapshot = nil, Don't use ! without checking.
            snapshot = nil
            setNeedsDisplay()
        default: break
        }
    }
    
    func moveOrigin(byReactingTo panRecognizer: UIPanGestureRecognizer) {
        switch panRecognizer.state {
        case .began:
            snapshot = self.snapshotView(afterScreenUpdates: false)
            snapshot!.alpha = 0.5
            self.addSubview(snapshot!)
        case .changed:
            let translation = panRecognizer.translation(in: self)
            if translation.x != 0 || translation.y != 0 {
                snapshot!.center.x += translation.x
                snapshot!.center.y += translation.y
                panRecognizer.setTranslation(CGPoint(x: 0, y: 0), in: self)
            }
        case .ended:
            origin.x += snapshot?.frame.origin.x ?? graphCenter.x
            origin.y += snapshot?.frame.origin.y ?? graphCenter.y
            snapshot!.removeFromSuperview()
            snapshot = nil
            setNeedsDisplay()
        default: break
        }
    }
    
    func setOrigin(byReactingTo tapRecognizer: UITapGestureRecognizer) {
        if tapRecognizer.state == .ended {
            origin = tapRecognizer.location(in: self)
            self.setNeedsDisplay()
        }
    }
    
}
