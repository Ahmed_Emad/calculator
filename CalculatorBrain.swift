//
//  CalculatorBrain.swift
//  Calculator
//
//  Created by Ahmed Barakat on 93 / 3 / 17.
//  Copyright © 2017 Ahmed Barakat. All rights reserved.
//

import Foundation

struct CalculatorBrain {
    
    private var operand: Operand?
    
    private var pendingBinaryAction: PendingBinaryAction?
    
    private var userActionsHandler = UserActionsHandler()
    
    private enum Operand {
        case value(Double)
        case variable(String)
    }
    
    private enum Operation {
        case constant(Double, String)
        case unaryOperation((Double) -> Double, (String) -> String)
        case binaryOperation((Double, Double) -> Double, (String, String) -> String)
        case clear
        case equals
    }
    
    private var operations: [String: Operation] = [
        "π": Operation.constant(Double.pi, "π"),
        "e": Operation.constant(M_E, "e"),
        "sin": Operation.unaryOperation(sin, { "sin(\($0))" }),
        "cos": Operation.unaryOperation(cos, { "cos(\($0))" }),
        "tan": Operation.unaryOperation(tan, { "tan(\($0))" }),
        "±": Operation.unaryOperation({ -$0 }, { "-(\($0))" }),
        "√": Operation.unaryOperation(sqrt, { "√(\($0))" }),
        "!": Operation.unaryOperation(CalculatorBrain.factorial, { "(\($0))!" }),
        "x": Operation.binaryOperation(*, { "(\($0)*\($1))" }),
        "÷": Operation.binaryOperation(/, { "(\($0)÷\($1))" }),
        "+": Operation.binaryOperation(+, { "(\($0)+\($1))" }),
        "-": Operation.binaryOperation(-, { "(\($0)-\($1))" }),
        "mod": Operation.binaryOperation({ $0.truncatingRemainder(dividingBy: $1) }, { "(\($0)mod\($1))" }),
        "^": Operation.binaryOperation(pow, { "(\($0)^\($1))" }),
        "C": Operation.clear,
        "=": Operation.equals,
        ]
    
    private static func factorial(_ n: Double) -> Double {
        if n <= 0 { return 1 }
        else { return n * factorial(n - 1) }
    }
    
    var result: Double? {
        let lastAction = userActionsHandler.peek()
        return lastAction?.result()
    }
    
    var resultIsPending: Bool {
        return pendingBinaryAction != nil
    }
    
    var description: String {
        let lastAction = userActionsHandler.peek()
        return lastAction?.description ?? ""
    }
    
    //TODO: Could we use And word here? lastActionAndInfo, you use 2 objects in tuple
    var lastActionInfo: (function: ((Double) -> (Double)), description: String)? {
        guard let lastAction = userActionsHandler.peek() else {
            return nil
        }
        switch lastAction {
        case let .unaryAction(_, valueGenerator, descriptionGenerator):
            return (valueGenerator, descriptionGenerator(""))
        case .constant(_, _), .value(_), .binaryAction(_, _):
            guard let result = evaluate().result else {
                return nil
            }
            return ({ (Double) -> Double in result }, String(result))
        default: return nil
        }
    }
    
    func evaluate(using variables: [String: Double]? = nil)
        -> (result: Double?, isPending: Bool, description: String) {
            let lastAction = userActionsHandler.peek()
            let result = lastAction?.result(with: variables)
            let isPending = pendingBinaryAction != nil
            let description: String
            if let pendingBinaryAction = pendingBinaryAction {
                description = pendingBinaryAction.description
            } else {
                description = lastAction?.description ?? ""
            }
            return (result, isPending, description)
    }
    
    mutating func performOperation(_ symbol: String) {
        guard let operation = operations[symbol] else {
            return
        }
        switch operation {
        case let Operation.constant(value, description):
            let constantAction = UserAction.constant(value, description)
            userActionsHandler.addAction(constantAction)
        case let Operation.unaryOperation(function, descriptionGenerator):
            addOperandToActions()
            guard let lastAction = userActionsHandler.peek() else {
                break
            }
            let unaryAction = UserAction.unaryAction(lastAction, function, descriptionGenerator)
            userActionsHandler.addAction(unaryAction)
        case let Operation.binaryOperation(function, descriptionGenerator):
            addOperandToActions()
            guard let lastAction = userActionsHandler.peek() else {
                break
            }
            pendingBinaryAction = PendingBinaryAction(firstAction: lastAction,
                                                      description: lastAction.description + symbol,
                                                      function: function,
                                                      descriptionGenerator: descriptionGenerator)
        case Operation.equals:
            addOperandToActions()
            guard let pendingBinaryAction = pendingBinaryAction,
                let secondAction = userActionsHandler.removeLast() else {
                    break
            }
            let binaryAction = UserAction.binaryAction(secondAction, pendingBinaryAction)
            userActionsHandler.addAction(binaryAction)
            clear()
        case Operation.clear:
            clear()
            userActionsHandler.clear()
        }
    }
    
    private mutating func clear() {
        pendingBinaryAction = nil
        operand = nil
    }
    
    @discardableResult
    private mutating func addOperandToActions() -> Bool {
        guard let firstOperand = operand else {
            return false
        }
        let operandAction: UserAction
        switch firstOperand {
        case let .value(value):
            operandAction = .value(value)
        case let .variable(name):
            operandAction = .variable(name)
        }
        userActionsHandler.addAction(operandAction)
        operand = nil
        return true
    }
    
    mutating func setOperand(_ value: Double) {
        operand = .value(value)
    }
    
    mutating func setOperand(variable name: String) {
        operand = .variable(name)
    }
    
    mutating func undo() {
        operand = nil
        //Completed: Use guard (reduce condition levels)
        guard pendingBinaryAction == nil else {
            pendingBinaryAction = nil
            return
        }
        if let lastAction = userActionsHandler.undo() {
            switch lastAction {
            case let UserAction.binaryAction(_, pendingBinaryAction):
                self.pendingBinaryAction = pendingBinaryAction
            default:
                self.pendingBinaryAction = nil
            }
        }
        
    }
    
    mutating func redo() {
        clear()
        userActionsHandler.redo()
    }
    
}
